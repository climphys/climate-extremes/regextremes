### This is the README for the regextRemes package

These are changes based on the R package "extremes":
Gilleland, E., & Katz, R. W. (2016). extRemes 2.0: An Extreme Value Analysis Package in R. Journal of Statistical Software, 72(8), 1–39. https://doi.org/10.18637/jss.v072.i08
